
container_php       = app
container_db        = db
container_webserver = webserver
container_nodejs    = nodejs

#############################################
###                                       ###
###   MakeFile for Laravel Crash Course   ###
###                                       ###
#############################################

composer_dep: #install composer dependency >> ./vendors
	@docker run --rm -v $(CURDIR)/app:/app composer install

laravel_install: #Create new Laravel project
	@docker-compose exec $(container_php) composer create-project --prefer-dist laravel/laravel app

key: #generate APP key
	@docker-compose exec $(container_php) php artisan key:generate

ownership: #Set ownership
	@sudo chown $(USER):$(USER) . -R

#####################################
###                               ###
###       Work in containers      ###
###                               ###
#####################################

start: #start docker containers @docker-compose up -d
	@docker-compose up -d

stop: #stop docker containers
	@docker-compose down

show: #show docker's containers
	@sudo docker ps

connect_app: #Connect to APP container
	@docker-compose exec $(container_php) bash

connect_db: #Connect to DB container
	@docker-compose exec $(container_db) bash

connect_server: #Connect to container_webserver container
	@docker-compose exec $(container_webserver) /bin/sh

connect_nodejs: #Connect
	@docker-compose exec $(container_nodejs) /bin/sh

#############################################
###                                       ###
###            Laravel things             ###
###                                       ###
#############################################

create_controller: #create controller name=[controllerName]
	@docker-compose exec $(container_php) php artisan make:controller $(name)Controller

create_model: #create controller name=[modelName]
	@docker-compose exec $(container_php) php artisan make:model Models/$(name) -m

create_seeder: #create seeder name=[seederName]
	@docker-compose exec $(container_php) php artisan make:seeder $(name)TableSeeder

create_request: #create FormRequest name=[controllerName]
	@docker-compose exec $(container_php) php artisan make:request $(name)Request

create_test: #create test name=[testName]
	@docker-compose exec $(container_php) php artisan make:test $(name)Test

create_provider: #create provider name=[providerName]
	@docker-compose exec $(container_php) php artisan make:provider $(name)Provider

#############################################
###                                       ###
###           Laravel services            ###
###                                       ###
#############################################

auth: #Refresh the database and run all database seeds
	@docker-compose exec $(container_php) php artisan make:auth

npm_i: #Run watch
	@docker-compose exec $(container_nodejs) npm install

watch: #Run watch
	@docker-compose exec $(container_nodejs) npm run watch

refresh: #Refresh the database and run all database seeds
	@docker-compose exec $(container_php) php artisan migrate:refresh --seed

tinker: #Run tinker
	@docker-compose exec $(container_php) php artisan tinker

composer_dump: #clear laravel cache
	@docker-compose exec $(container_php) bash -c 'php composer.phar dump-autoload'

clear_cache: #clear laravel cache
	@docker-compose exec $(container_php) bash -c 'php artisan cache:clear && php artisan view:clear && php artisan route:clear && php artisan config:clear'

test: #test
	@docker-compose exec $(container_php) bash -c 'vendor/bin/phpunit'