<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get("/", "CategoriesController@index")->name('categories');

Route::post("/", "CategoriesController@create")->name('save_category');
Route::delete("/{id}", "CategoriesController@delete")->name('delete_category')->where('id', '[0-9]+');
Route::put("/{id}", "CategoriesController@update")->name('update_category')->where('id', '[0-9]+');
