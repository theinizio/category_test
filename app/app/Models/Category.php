<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class Category extends Model
{
    protected $fillable = ['name', 'parent_id'];

    /**
     * @return string
     */
    public function getAllignedArray()
    {
        $unaligned = $this->get();
        if (count(($unaligned)) > 0) {
            $cats = [];
            foreach ($unaligned as $cat) {
                $cat = collect($cat->attributes)->only(['id', 'parent_id', 'name']);
                $cats[$cat['parent_id']][$cat['id']] = $cat;
            }
            $result = '{"name" : "My Tree", "children" : [' . $this->build_tree($cats, 0) . ']}';
        }

        return $result;
    }
    /**
     * @param $cats
     * @param $parent_id
     * @return string
     */
    public function build_tree($cats, $parent_id): ?string
    {
        if (is_array($cats) and isset($cats[$parent_id])) {
            $result = '';
            foreach ($cats[$parent_id] as $cat) {
                $children = $this->build_tree($cats, $cat['id']);
                if ($cat['name'] != null && $children != null) {
                    $result .= '{"id":'.$cat['id'].', "name": "' . $cat['name'] . '", "children":[' . $children . ']},';
                }
                if ($cat['name'] != null && $children == null) {
                    $result .= '{"id":'.$cat['id'].', "name": "' . $cat['name'] . '"},';
                }
                if ($cat['name'] == null && $children != null) {
                    $result .= '"children": [' . $children . '],';
                }
            }
        } else return null;

        return $result;
    }

    /**
     * @param $data
     * @return Category
     */
    public function addCategory($data):Category
    {
        return $this->firstOrCreate([
            "parent_id" => $data['id'],
            "name" => $data['name']
        ]);

    }
    public function deleteCategory(int $id)
    {
        $category = $this->findOrFail($id);

        $this->deleteChildren($id);
        $category->delete();
        return $category;
    }

    private function deleteChildren(int $id){
        $children=$this->getChildren($id);
        if($children->count()){
            foreach($children as $child){
                $this->deleteChildren($child->id);
                $child->delete();
            }
        }
    }

    private function getChildren(int $id):Collection
    {
        return $this->where("parent_id", $id)->get();
    }
}
