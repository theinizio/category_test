<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    /**
     * @var \Illuminate\Support\Collection
     */
    protected $routes;

    /**
     * PartnerController constructor.
     */
    public function __construct()
    {

    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Partner $category
     * @return \Illuminate\View\View
     */
    public function index(Request $request, Category $category)
    {
        return view('index', [
            'categories' => $category->getAllignedArray(),

        ]);
    }


    public  function create(Request $request, Category $category){
        try{
           return response()->json($category->addCategory($request->only(['id', 'name'])));
            //return response()->json(["result" => "successful"], 200);
        } catch (Exception $exception) {
            return response()->json($exception->getMessage(), 400);
        }
    }
    /**
     * @param PartnerUpdateRequest $request
     * @param Partner $partner
     * @return JsonResponse
     */
    public function update(int $id, Request $request, Category $category): JsonResponse
    {
        try {
            $category->updateOrCreate(["id" => $request->id], ["name" => $request->name]);
            return response()->json(["result" => "ok"], 200);
        } catch (Exception $exception) {
            return response()->json($exception->getMessage(), 400);
        }
    }

    /**
     * @param $id
     * @param Partner $partner
     * @return JsonResponse
     */
    public function delete($id, Category $category): JsonResponse
    {
        try {
            $category->deleteCategory($id);
            return response()->json(["result" => "ok"], 200);
        } catch (Exception $exception) {
            return response()->json($exception->getMessage(), 400);
        }
    }

}
